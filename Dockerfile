FROM alpine:3.11

ARG ANSIBLE_VERSION=2.9.11
ARG CRYPTOGRAPHY_DONT_BUILD_RUST=1

RUN apk add --no-cache \
		python3=~3.8 \
		openssl=~1.1 \
		ca-certificates=~20191127 \
		bash=~5 \
		gettext=~0.20 && \
	apk add --no-cache --virtual build-dependencies \
		python3-dev=~3.8 \
		build-base=~0.5 \
		libffi-dev=~3 \
		openssl-dev=~1.1 && \
	pip3 install --no-cache-dir ansible==${ANSIBLE_VERSION} \
		pywinrm==0.2.2  && \
	apk del build-dependencies

COPY rootfs /

ENTRYPOINT ["/usr/bin/generate-inventory"]
CMD ["ansible-playbook"]
