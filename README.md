# Configuración del servidor
La instalación de Ansible es necesario configurar WinRM.

Abrir PowerShell en modo administrador y ejecutar el siguiente script
```
$url = "https://raw.githubusercontent.com/ansible/ansible/devel/examples/scripts/ConfigureRemotingForAnsible.ps1"
$file = "$env:temp\ConfigureRemotingForAnsible.ps1"

[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
$down = New-Object System.Net.WebClient

$down.DownloadFile($url, $file)

powershell.exe -ExecutionPolicy ByPass -File $file
```
